#include <iostream>

void	upper(char *str)
{
  int	i;
  char	a;

  i = 0;
  while(str[i] != '\0')
    {
      a = toupper((int)str[i]);
      std::cout << a;
      i++;
    }
}

int	main(int argc, char **argv)
{
  int	i;

  i = 1;
  if (argc >= 2)
    {
      while(i <= (argc - 1))
	{
	  upper(argv[i]);
	  if(i != (argc - 1))
	    std::cout << " ";
	  i++;
	}
      std::cout << std::endl;
    }
  else
    std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
  return 0;
}
