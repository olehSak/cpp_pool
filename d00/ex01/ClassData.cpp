// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ClassData.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/09/30 12:56:46 by osak              #+#    #+#             //
//   Updated: 2019/09/30 12:56:47 by osak             ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ClassUser.hpp"

UserData::UserData()
{
	return;
}
UserData::~UserData()
{
	return;
}

std::string UserData::getInput()
{
	std::string input;

	std::getline(std::cin, input);
	if(std::cin.eof())
	{
		std::cout << "CTRL-D input -> break" << std::endl;
		exit(1);
	}
	return input;
}

std::string	UserData::cutter(std::string data)
{
	if(data.size() > 10)
	{
		data.resize(10);
		data[9] = '.';
	}
	return data;
}

std::string	UserData::fsearch(int i)
{
	std::string output;

	if(i == 1)
		output = this->cutter(this->firstName);
	else if(i ==2)
		output = this->cutter(this->secondName);
	else if(i == 3)
		output = this->cutter(this->nickname);
	return output;
}

void	UserData::fadd()
{
	std::string str;

	std::getline(std::cin, str);
	this->setFirstName();
	this->setSecondName();
	this->setNickname();
  	this->setLogin();
  	this->setPostAddress();
  	this->setEmail();
  	this->setPhone();
  	this->setBirthdate();
	this->setFavoriteMeal();
	this->setUnderwearColor();
  	this->setDarkessSecret();
	std::cout << "COMPLETE" << std::endl;
}

void	UserData::printAllUserData()
{
	std::cout << this->getFirstName() << std::endl;
	std::cout << this->getSecondName() << std::endl;
	std::cout << this->getNickname() << std::endl;
	std::cout << this->getLogin() << std::endl;
	std::cout << this->getPostAddress() << std::endl;
	std::cout << this->getEmail() << std::endl;
	std::cout << this->getPhone() << std::endl;
	std::cout << this->getBirthdate() << std::endl;
	std::cout << this->getFavoriteMeal() << std::endl;
	std::cout << this->getUnderwearColor() << std::endl;
	std::cout << this->getDarkessSecret() << std::endl;
}

void UserData::setFirstName()
{
	std::cout << "Set first name:" << std::endl;
	this->firstName = getInput();
}

void UserData::setSecondName()
{
	std::cout << "Set second name:" << std::endl;
	this->secondName = getInput();
}

void UserData::setNickname()
{
	std::cout << "Set nickname:" << std::endl;
	this->nickname = getInput();
}

void UserData::setLogin()
{
	std::cout << "Set login:" << std::endl;
	this->login = getInput();
}

void UserData::setPostAddress()
{
	std::cout << "Set post address:" << std::endl;
	this->postAdress = getInput();
}

void UserData::setEmail()
{
	std::cout << "Set email:" << std::endl;
	this->email = getInput();
}

void UserData::setPhone()
{
	std::cout << "Set phone number:" << std::endl;
	this->phone = getInput();
}

void	UserData::setBirthdate()
{
	std::cout << "Input birthday date dd mm yyyy:" << std::endl;
	this->date = getInput();
}

void UserData::setFavoriteMeal()
{
	std::cout << "Set favorite meal:" << std::endl;
	this->favMeal = getInput();
}

void UserData::setUnderwearColor()
{
	std::cout << "Set underwear color:" << std::endl;
	this->underwearCol = getInput();
}

void UserData::setDarkessSecret()
{
	std::cout << "Set darkess secret:" << std::endl;
	this->darkestSecret = getInput();
}

std::string	UserData::getFirstName()
{
	return this->firstName;
}

std::string	UserData::getSecondName()
{
	return this->secondName;
}

std::string	UserData::getNickname()
{
	return this->nickname;
}

std::string	UserData::getLogin()
{
	return this->login;
}

std::string	UserData::getPostAddress()
{
	return this->postAdress;
}

std::string	UserData::getEmail()
{
	return this->email;
}


std::string	UserData::getPhone()
{
	return this->phone;
}

std::string	UserData::getBirthdate()
{
	return this->date;
}

std::string	UserData::getFavoriteMeal()
{
	return this->favMeal;
}

std::string	UserData::getUnderwearColor()
{
	return this->underwearCol;
}

std::string	UserData::getDarkessSecret()
{
	return this->darkestSecret;
}


