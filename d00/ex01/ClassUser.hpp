#ifndef CLASSUSER_HPP
#define CLASSUSER_HPP

# include <string>
# include <iostream>
# include <iomanip>

class UserData
{
public:
	UserData();
	~UserData();

	void		fadd();
	std::string	fsearch(int i);
	std::string	cutter(std::string data);
	void		printAllUserData();

	void		setFirstName();
	void		setSecondName();
	void		setNickname();
	void		setLogin();
	void		setPostAddress();
	void		setEmail();
	void		setPhone();
	void		setBirthdate();
	void		setFavoriteMeal();
	void		setUnderwearColor();
	void		setDarkessSecret();

	std::string		getInput();

	std::string		getFirstName();
	std::string		getSecondName();
	std::string		getNickname();
	std::string		getLogin();
	std::string		getPostAddress();
	std::string		getEmail();
	std::string		getPhone();
	std::string		getBirthdate();
	std::string		getFavoriteMeal();
	std::string		getUnderwearColor();
	std::string		getDarkessSecret();
private:
	std::string	firstName;
	std::string	secondName;
	std::string	nickname;
	std::string	login;
	std::string	postAdress;
	std::string	email;
	std::string	phone;
	std::string	date;
	std::string	favMeal;
	std::string	underwearCol;
	std::string	darkestSecret;
};

void	printAllUserData(UserData *users, int i);

#endif
