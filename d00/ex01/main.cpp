// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/09/30 12:54:42 by osak              #+#    #+#             //
//   Updated: 2019/09/30 12:54:46 by osak             ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ClassData.cpp"

int	main(void)
{
  std::string	input;
  UserData 		users[8];
  int	i;
  int	x;

  i = 0;
  std::cout << "Hello." << std::endl;
  std::cout << "You should accept the ADD command, the SEARCH command or the EXIT command." << std::endl;
  std::string exit("EXIT"), search("SEARCH"), add("ADD");

  while (input != exit)
  {
  	std::cin >> input;
      if(std::cin.eof())
      {
      	std::cout << "CTRL-D input -> break" << std::endl;
		  break;
      }
      else if(input == add && i < 8)
      {
      	input.clear();
      	std::cin.clear();
      	users[i++].fadd();
      }
      else if(input == search)
      {
      	int a = 0;
      	if (i == 0)
      	{
			std::cout << "No have contacts" << std::endl;
		}
      	else
      		{
				std::cout << std::setw(10)  << "INDEX" << '|' <<
				std::setw(10) << "F_NAME"<< '|' <<
				std::setw(10) << "L_NAME"<< '|' <<
				std::setw(10) << "NICKNAME" << '|' << std::endl;
			while (a < i)
			{
				std::cout << std::setw(10)  << a+1 << '|'
				<< std::setw(10) << users[a].fsearch(1) << '|'
				<< std::setw(10) << users[a].fsearch(2) << '|'
				<< std::setw(10) << users[a].fsearch(3) << '|' << std::endl;
				a++;
			}
			std::cout << "Input 1-8 number, if have for details" << std::endl;
			std::cin >> x;
			if (x > 0 && x <= i)
			{
				users[x - 1].printAllUserData();
			}
			else
				{
					std::cin.clear();
			}
		}
      }
      else if (input == exit)
		  break;
      else
      	{
		  std::cout << "Wrong command." << std::endl;
		  if (input == add && i == 8)
		  	std::cout << "Phonebook is full." << std::endl;
	  	}
    }
  return (0);
}
