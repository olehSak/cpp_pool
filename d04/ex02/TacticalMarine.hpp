//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef CPP_POOL_TACTICALMARINE_HPP
#define CPP_POOL_TACTICALMARINE_HPP

#include "ISpaceMarine.hpp"

class TacticalMarine : public ISpaceMarine {
private:

public:
	TacticalMarine();
	~TacticalMarine();
	TacticalMarine(TacticalMarine const &src);

	TacticalMarine	&operator=(TacticalMarine const &rhs);
	ISpaceMarine* clone() const;
	void battleCry() const;
	void rangedAttack() const;
	void meleeAttack() const;
};

#endif //CPP_POOL_TACTICALMARINE_HPP
