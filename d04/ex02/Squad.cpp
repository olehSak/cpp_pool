//
// Created by Oleh SAK on 2019-10-05.
//

#include "Squad.hpp"

Squad::Squad() : _list(0), _last(0), _listSize(0){
	_list = 0;
};

Squad::Squad(Squad const& rhs): _list(0), _last(0), _listSize(0) {

	int size = rhs.getCount();

	for(int i = 0; i < size; i++)
		this->push(rhs.getUnit(i)->clone());
};

Squad& Squad::operator=(Squad const& rhs) {

	struct _marine* tmp;

	while (_list) {
		tmp = _list;
		_list = _list->next;
		delete tmp->unit;
		delete tmp;
	}
	delete _list;

	int size = rhs.getCount();

	for(int i = 0; i < size; i++)
		this->push(rhs.getUnit(i)->clone());

	return *this;
};

Squad::~Squad() {

	struct _marine* tmp;

	while (_list) {
		tmp = _list;
		_list = _list->next;
		delete tmp->unit;
		delete tmp;
	}
	delete _list;
};

int           Squad::getCount() const {
	return _listSize;
};

int           Squad::push(ISpaceMarine* unit) {

	if (unit && !checkDup(unit)) {

		if (!_list)
		{
			_list = new _marine();
			_list->unit = unit;
			_list->next = 0;
			_last = _list;
		} else {
			_last->next = new _marine();
			_list->next->unit = unit;
			_list->next->next = 0;
			_last = _last->next;
		}
		_listSize++;
	}
	return _listSize;
};

bool          Squad::checkDup(ISpaceMarine* unit) {

	_marine *head = _list;

	while (head) {
		if (head->unit == unit)
			return true;
		head = head->next;
	}
	return  false;
}

ISpaceMarine* Squad::getUnit(int n) const {

	if (n >= _listSize || !_list)
		return 0;

	struct _marine* head = _list;

	int index = 0;

	while(head){
		if (index == n)
			return head->unit;
		head = head->next;
		index++;
	}
	return 0;
};
