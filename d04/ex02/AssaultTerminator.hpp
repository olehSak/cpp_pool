//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef CPP_POOL_ASSAULTTERMINATOR_HPP
#define CPP_POOL_ASSAULTTERMINATOR_HPP

#include "ISpaceMarine.hpp"

class AssaultTerminator : public ISpaceMarine {
public:
	AssaultTerminator();
	~AssaultTerminator();
	AssaultTerminator(AssaultTerminator const &src);

	AssaultTerminator	&operator=(AssaultTerminator const &rhs);
	ISpaceMarine* clone() const;
	void battleCry() const;
	void rangedAttack() const;
	void meleeAttack() const;
};

#endif //CPP_POOL_ASSAULTTERMINATOR_HPP
