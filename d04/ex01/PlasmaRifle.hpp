//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef PLASMARIFLE_HPP
#define PLASMARIFLE_HPP

# include <string>
# include <iostream>
# include "AWeapon.hpp"

class PlasmaRifle: public AWeapon
{
public:
	PlasmaRifle();
	~PlasmaRifle();
	PlasmaRifle(PlasmaRifle const &src);

	PlasmaRifle				&operator=(PlasmaRifle const &rhs);

	void					attack() const;
};

#endif
