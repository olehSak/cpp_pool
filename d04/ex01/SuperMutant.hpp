//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef SUPERMUTANT_HPP
#define SUPERMUTANT_HPP

#include <string>
#include <iostream>
#include "Enemy.hpp"

class SuperMutant: public Enemy
{
public:
	SuperMutant();
	~SuperMutant();
	SuperMutant(SuperMutant const &src);

	SuperMutant				&operator=(SuperMutant const &rhs);

	virtual void		takeDamage(int damage);
};


#endif
