//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef ENEMY_HPP
#define ENEMY_HPP

#include <string>

class Enemy
{
protected:
	std::string		_type;
	int				_hp;

public:
	Enemy(int hp, std::string const &type);
	virtual ~Enemy();
	Enemy(Enemy const &src);

	Enemy				&operator=(Enemy const &rhs);

	std::string	const	&getType() const;
	int					getHP() const;
	virtual void		takeDamage(int damage);
};

#endif
