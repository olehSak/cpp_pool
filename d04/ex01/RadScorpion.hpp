//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef RADSCORPION_HPP
#define RADSCORPION_HPP

#include <string>
#include <iostream>
#include "Enemy.hpp"

class RadScorpion: public Enemy
{
public:
	RadScorpion();
	~RadScorpion();
	RadScorpion(RadScorpion const &src);

	RadScorpion				&operator=(RadScorpion const &rhs);
};

#endif
