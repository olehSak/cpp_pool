//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef POWERFIST_HPP
#define POWERFIST_HPP


#include "AWeapon.hpp"
#include <string>
#include <iostream>

class PowerFist: public AWeapon
{
public:
	PowerFist(void);
	~PowerFist(void);
	PowerFist(PowerFist const &src);

	PowerFist				&operator=(PowerFist const &rhs);

	void					attack() const;
};


#endif
