//
// Created by Oleh SAK on 2019-10-05.
//

#include "PlasmaRifle.hpp"

PlasmaRifle::PlasmaRifle():
		AWeapon("Plasma Rifle", 5, 21)
{}

PlasmaRifle::PlasmaRifle(PlasmaRifle const &src):
		AWeapon("Plasma Rifle", 5, 21)
{
	*this = src;
}

PlasmaRifle::~PlasmaRifle()
{}

PlasmaRifle				&PlasmaRifle::operator=(PlasmaRifle const	&rhs)
{
	if (this != &rhs)
		AWeapon::operator=(rhs);
	return (*this);
}

void					PlasmaRifle::attack() const
{
	std::cout << "* piouuu piouuu piouuu *" << std::endl;
}
