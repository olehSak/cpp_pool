//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef CPP_POOL_CURE_HPP
#define CPP_POOL_CURE_HPP

#include "ICharacter.hpp"

class Cure: public AMateria
{
public:
	Cure();
	~Cure();
	Cure(Cure const &src);

	Cure	&operator=(Cure const &rhs);

	AMateria	*clone() const;
	void		use(ICharacter &target);
};

#endif //CPP_POOL_CURE_HPP
