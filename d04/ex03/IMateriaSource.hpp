//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef CPP_POOL_IMATERIASOURCE_HPP
#define CPP_POOL_IMATERIASOURCE_HPP

#include "AMateria.hpp"

class IMateriaSource
{
public:
	virtual ~IMateriaSource() {}
	virtual void learnMateria(AMateria*) = 0;
	virtual AMateria* createMateria(std::string const & type) = 0;
};

#endif //CPP_POOL_IMATERIASOURCE_HPP
