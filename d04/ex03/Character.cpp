//
// Created by Oleh SAK on 2019-10-05.
//

#include "Character.hpp"

Character::Character(std::string const &name): _name(name)
{

	for (int i = 0; i < 4; i++) {
		this->_materias[i] = NULL;
	}
}

Character::Character(Character const &src)
{
	*this = src;
}

Character::~Character()
{

}

Character			&Character::operator=(Character const &rhs)
{
	int				i;
	AMateria		*cpy;

	if (this != &rhs)
	{
		i = 0;
		while (i < 4)
		{
			if (this->_materias[i])
			{
				cpy = this->_materias[i]->clone();
				delete this->_materias[i];
				this->_materias[i] = cpy;
			}
			i++;
		}
		this->_name = rhs._name;
	}
	return (*this);
}

void				Character::equip(AMateria *materia)
{
	for (int i = 0; i < 4; i ++) {
		if (!this->_materias[i]) {
			this->_materias[i] = materia;
			return;
		}
	}
}

void				Character::unequip(int idx)
{
	if (idx > -1 && idx < 4)
		this->_materias[idx] = NULL;
}

void				Character::use(int idx, ICharacter &target)
{
	if (idx > -1 && idx < 4 && this->_materias[idx] != NULL)
		this->_materias[idx]->use(target);
}

std::string const	&Character::getName() const {
	return (this->_name);
}
