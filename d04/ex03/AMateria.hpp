//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef CPP_POOL_AMATERIA_HPP
#define CPP_POOL_AMATERIA_HPP

#include "ICharacter.hpp"

class ICharacter;

class AMateria
{
private:
	std::string		_type;
	unsigned int	_xp;

public:
	AMateria(std::string const &type);
	virtual ~AMateria(void);
	AMateria(AMateria const &src);

	AMateria			&operator=(AMateria const &rhs);

	virtual AMateria	*clone() const = 0;
	virtual void		use(ICharacter &target);

	std::string const	&getType() const;
	unsigned int		getXP() const;
};

#endif //CPP_POOL_AMATERIA_HPP
