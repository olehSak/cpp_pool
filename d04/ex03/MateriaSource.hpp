//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef CPP_POOL_MATERIASOURCE_HPP
#define CPP_POOL_MATERIASOURCE_HPP

#include "IMateriaSource.hpp"

class MateriaSource: public IMateriaSource
{
private:
	int					_materia_count;
	AMateria			*_materias[4];

public:
	MateriaSource();
	~MateriaSource();
	MateriaSource(MateriaSource const &src);

	MateriaSource		&operator=(MateriaSource const &rhs);

	void				learnMateria(AMateria *materia);
	AMateria			*createMateria(std::string const &type);
};

#endif //CPP_POOL_MATERIASOURCE_HPP
