//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef CPP_POOL_CHARACTER_HPP
#define CPP_POOL_CHARACTER_HPP

#include "ICharacter.hpp"

class Character: public ICharacter
{
private:
	std::string		_name;
	AMateria		*_materias[4];

public:
	Character(std::string const &name);
	~Character();
	Character(Character const &src);

	Character			&operator=(Character const &rhs);

	void				equip(AMateria *materia);
	void				unequip(int idx);
	void				use(int idx, ICharacter &target);

	std::string const	&getName() const;
};

#endif //CPP_POOL_CHARACTER_HPP
