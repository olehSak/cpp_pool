//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef CPP_POOL_ICHARACTER_HPP
#define CPP_POOL_ICHARACTER_HPP

#include <string>
#include <iostream>
#include "AMateria.hpp"

class AMateria;

class ICharacter
{
public:
	virtual ~ICharacter() {}
	virtual std::string const & getName() const = 0;
	virtual void equip(AMateria* m) = 0;
	virtual void unequip(int idx) = 0;
	virtual void use(int idx, ICharacter& target) = 0;
};

#endif //CPP_POOL_ICHARACTER_HPP
