//
// Created by Oleh SAK on 2019-10-05.
//

#include "AMateria.hpp"

#include "ICharacter.hpp"

AMateria::AMateria(std::string const &type): _type(type)
{
	this->_xp = 0;
}

AMateria::AMateria(AMateria const &src)
{
	*this = src;
}

AMateria::~AMateria(void)
{}

AMateria			&AMateria::operator=(AMateria const &rhs)
{
	if (this != &rhs)
	{
		this->_type = rhs._type;
		this->_xp = rhs._xp;
	}
	return (*this);
}

void				AMateria::use(ICharacter &character)
{
	character.equip(this);
	_xp += 10;
}

std::string const	&AMateria::getType() const {
	return (this->_type);
}

unsigned int		AMateria::getXP() const {
	return (this->_xp);
}
