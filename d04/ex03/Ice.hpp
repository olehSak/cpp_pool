//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef CPP_POOL_ICE_HPP
#define CPP_POOL_ICE_HPP

#include "AMateria.hpp"
#include <iostream>

class Ice: public AMateria
{
public:
	Ice();
	~Ice();
	Ice(Ice const &src);

	Ice					&operator=(Ice const &rhs);

	AMateria			*clone() const;
	void				use(ICharacter &target);
};

#endif //CPP_POOL_ICE_HPP
