//
// Created by Oleh SAK on 2019-10-04.
//

#include "Sorcerer.hpp"

Sorcerer::Sorcerer(std::string name, std::string title): _name(name), _title(title)
{
	std::cout << this->_name << ", " << this->_title << ", is born !" << std::endl;
}

Sorcerer::Sorcerer(Sorcerer const &src)
{
	*this = src;
	std::cout << this->_name << ", " << this->_title << ", is born !" << std::endl;
}

Sorcerer::~Sorcerer()
{
	std::cout << this->_name << ", " << this->_title << ", is dead. Consequences will never be the same !" << std::endl;
}

Sorcerer			&Sorcerer::operator=(Sorcerer const	&rhs)
{
	if (this != &rhs)
	{
		this->_name = rhs._name;
		this->_title = rhs._title;
	}
	return (*this);
}

std::ostream	&operator<<(std::ostream &out, Sorcerer const &rhs)
{
	out << "I am " << rhs.getName()
		<< ", " << rhs.getTitle()
		<< ", and I like ponies !" << std::endl;
	return (out);
}

void Sorcerer::polymorph(Victim const & victim) const {
	victim.getPolymorphed();
}

std::string const& Sorcerer::getName() const {
	return _name;
}

std::string const& Sorcerer::getTitle() const {
	return _title;
}
