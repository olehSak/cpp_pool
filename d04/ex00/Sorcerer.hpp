#ifndef SORCERER_HPP
#define SORCERER_HPP

#include <iostream>
#include <string>
#include "Victim.hpp"

class Sorcerer {
protected:
	std::string _name;
	std::string	_title;

public:
	Sorcerer(std::string name, std::string title);
	~Sorcerer();
	Sorcerer(Sorcerer const & src);

	Sorcerer &	operator=(Sorcerer const & rhs);

	void polymorph(Victim const &) const;

	std::string const 	&getName() const;
	std::string const 	&getTitle() const;
};

std::ostream		&operator<<(std::ostream &out, Sorcerer const &rhs);

#endif
