//
// Created by Oleh SAK on 2019-10-05.
//

#include "Victim.hpp"

Victim::Victim(std::string name): _name(name) {
	std::cout << "Some random victim called " << this->_name << " just popped !" << std::endl;
}

Victim::Victim(Victim const &src)
{
	*this = src;
	std::cout << "Some random victim called " << this->_name << " just popped !" << std::endl;
}

Victim::~Victim() {
	std::cout << "Victim " << this->_name << " just died for no apparent reason !" << std::endl;
}

Victim				&Victim::operator=(Victim const	&rhs)
{
	if (this != &rhs)
		this->_name = rhs._name;
	return (*this);
}

std::string const& Victim::getName() const {
	return _name;
}

void Victim::getPolymorphed() const {
	std::cout << this->_name << " has been turned into a cute little sheep !" << std::endl;
}

std::ostream	&operator<<(std::ostream &out, Victim const &rhs)
{
	out << "I'm " << rhs.getName() << " and I like otters !" << std::endl;
	return (out);
}