//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef PEON_HPP
#define PEON_HPP

#include "Victim.hpp"
# include <string>
# include <iostream>

class Peon : public Victim {
public:
	Peon(std::string name);
	~Peon();
	Peon(Peon const &src);

	Peon	&operator=(Peon const &rhs);

	void	getPolymorphed() const;
};

#endif
