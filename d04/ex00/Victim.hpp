//
// Created by Oleh SAK on 2019-10-05.
//

#ifndef VICTIM_HPP
#define VICTIM_HPP

# include <string>
# include <iostream>

class Victim {
protected:
	std::string _name;

public:
	Victim(std::string name);
	~Victim();
	Victim(Victim const &src);

	Victim				&operator=(Victim const &rhs);

	virtual void getPolymorphed() const;

	std::string const 	&getName() const;
};

std::ostream	&operator<<(std::ostream &out, Victim const &rhs);

#endif
