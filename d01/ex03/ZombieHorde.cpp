#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int N)
{
	std::cout << "Horde Constructor called" << std::endl;
	this->count = N;
	this->zombie = new Zombie[N];

	std::string		name[3] = {"Mary", "Jane","John"};

	for(int i = 0; i < N; i++)
	{
		zombie[i].setName(name[std::rand() % 3]);
		zombie[i].setType("Angry");
	}
}

ZombieHorde::~ZombieHorde()
{
	delete [] zombie;
	std::cout << "Horde destructor called" << std::endl;
}

void	ZombieHorde::announce()
{
	for(int i = 0; i < this->count; i++)
	{
		this->zombie[i].announce();
	}
}

