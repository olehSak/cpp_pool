// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Zombie.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/10/01 17:58:27 by osak              #+#    #+#             //
//   Updated: 2019/10/01 17:58:29 by osak             ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Zombie.hpp"
#include "ZombieHorde.cpp"

Zombie::Zombie()
{
	std::cout << "Constructor called" << std::endl;
}

Zombie::~Zombie()
{
	std::cout << "Destructor called" << std::endl;
}

void	Zombie::announce()
{
	std::cout << "<" + this->getName() << " (" << this->getType() << ")> " << (std::rand() % 5) << "Braiinzzzzzz..." << std::endl;
}

void		Zombie::setName(std::string data)
{
	this->_name = data;
}

void 		Zombie::setType(std::string data)
{
	this->_type = data;
}

std::string	Zombie::getName(void)
{
	return this->_name;
}

std::string	Zombie::getType(void)
{
	return this->_type;
}
