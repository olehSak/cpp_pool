#ifndef ZOMBIEHORDE_HPP
#define ZOMBIEHORDE_HPP


class ZombieHorde {
public:
	ZombieHorde(int N);
	~ZombieHorde();
	void			announce();

private:
	Zombie *zombie;
	int 	count;
};


#endif
