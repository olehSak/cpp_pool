// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Zombie.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/10/01 17:57:13 by osak              #+#    #+#             //
//   Updated: 2019/10/01 17:57:15 by osak             ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ZOMBIE_HPP
#define ZOMBIE_HPP

#include <string>
#include <iostream>
#include <iomanip>

class Zombie
{
public:
  Zombie();
  ~Zombie();

  void			announce();
  void			setName(std::string);
  void			setType(std::string);
  std::string	getName();
  std::string	getType();
private:
  std::string _name;
  std::string _type;
};

#endif
