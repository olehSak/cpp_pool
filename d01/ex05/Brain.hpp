#ifndef BRAIN_HPP
#define BRAIN_HPP

#include <string>
#include <sstream>
#include <iostream>

class Brain
{
private:
	std::string	_address;

public:
	Brain ();
	std::string	identify() const;
	~Brain();
};


#endif
