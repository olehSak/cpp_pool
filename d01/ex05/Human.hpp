#ifndef HUMAN_HPP
#define HUMAN_HPP

#include "Brain.cpp"

class Human
{
private:
	const Brain brain;

public:
	std::string	identify() const;
	const Brain &getBrain() const;
};

#endif
