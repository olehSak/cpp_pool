//
// Created by Oleh SAK on 2019-10-02.
//

#ifndef CPP_POOL_HUMANB_HPP
#define CPP_POOL_HUMANB_HPP

#include <string>
#include "Weapon.hpp"
#include "HumanA.hpp"

class HumanB
{
private:
	std::string	_name;
	Weapon		*_weapon;

public:
	HumanB(const char *name);
	void setWeapon(Weapon &weapon);
	void	attack();
};

#endif //CPP_POOL_HUMANB_HPP
