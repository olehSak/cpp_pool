//
// Created by Oleh SAK on 2019-10-02.
//

#include "Weapon.hpp"

Weapon::Weapon(const char *type): _type(type)
{

}

const std::string &Weapon::getType()
{
	return (this->_type);
}

void Weapon::setType(const char *type)
{
	this->_type = type;
}