//
// Created by Oleh SAK on 2019-10-02.
//

#include "HumanB.hpp"

HumanB::HumanB(const char *name): _name(name), _weapon(0)
{
}

void HumanB::setWeapon(Weapon &weapon)
{
	this->_weapon = &weapon;
}

void	HumanB::attack()
{
	std::cout << _name << " attacks with his " << _weapon->getType() << std::endl;
}