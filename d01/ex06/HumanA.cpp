//
// Created by Oleh SAK on 2019-10-02.
//

#include "HumanA.hpp"

HumanA::HumanA(const char *name):
		_name(name), _weapon("knife")
{
}

HumanA::HumanA(const char *name, Weapon weapon):
		_name(name), _weapon(weapon)
{
}

void	HumanA::attack()
{
	std::cout << _name << " attacks with his " << _weapon.getType() << std::endl;
}
