//
// Created by Oleh SAK on 2019-10-02.
//

#ifndef CPP_POOL_HUMANA_HPP
#define CPP_POOL_HUMANA_HPP

#include <string>
#include <iostream>
#include "Weapon.hpp"

class HumanA
{
private:
	std::string	_name;
	Weapon		_weapon;

public:
	HumanA(const char *name, Weapon weapon);
	HumanA(const char *name);
	void	attack();

};


#endif //CPP_POOL_HUMANA_HPP
