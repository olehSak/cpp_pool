// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Pony.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/10/01 17:44:29 by osak              #+#    #+#             //
//   Updated: 2019/10/01 17:44:32 by osak             ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Pony.hpp"

Pony::Pony(std::string name, std::string color, std::string horn, std::string wings)
{
	putName(name);
	putColor(color);
	putHorn(horn);
	putWings(wings);
}

Pony::~Pony()
{
	std::cout << "Destructor clean memory." << std::endl;
}

void	Pony::putName(std::string name)
{
	this->name = name;
}

void	Pony::putColor(std::string data)
{
	this->color = data;
}

void	Pony::putHorn(std::string data)
{
	this->horn = data;
}

void	Pony::putWings(std::string data)
{
	this->wings = data;
}

std::string	Pony::getName()
{
	return this->name;
}

std::string	Pony::getColor()
{
	return this->color;
}

std::string	Pony::getHorn()
{
	return this->horn;
}

std::string	Pony::getWings()
{
	return this->wings;
}

