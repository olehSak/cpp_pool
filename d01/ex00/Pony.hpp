// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Pony.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/10/01 17:44:50 by osak              #+#    #+#             //
//   Updated: 2019/10/01 17:44:51 by osak             ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PONY_HPP
#define PONY_HPP

#include <iostream>
#include <string>

class Pony
{
public:
	Pony(std::string name, std::string color, std::string horn, std::string wings);
	~Pony();
	void	putName(std::string name);
	void	putColor(std::string);
	void	putHorn(std::string);
	void	putWings(std::string);
	std::string	getName();
	std::string	getColor();
	std::string	getHorn();
	std::string	getWings();
private:
	std::string name;
	std::string color;
	std::string horn;
	std::string wings;
};

#endif
