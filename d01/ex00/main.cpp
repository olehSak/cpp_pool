// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/10/01 17:44:38 by osak              #+#    #+#             //
//   Updated: 2019/10/01 17:44:41 by osak             ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Pony.cpp"

void 	ponyOnTheHeap(std::string name, std::string color, std::string horn, std::string wings)
{
	Pony *HeapPony = new Pony(name, color, horn, wings);
	std::cout << "My name is " << HeapPony->getName() <<", my color is " << HeapPony->getColor() <<
	". horn? " << horn << ". wings? " << wings << std::endl;
	delete(HeapPony);
}

void 	ponyOnTheStack(std::string name, std::string color, std::string horn, std::string wings)
{
	Pony StackPony(name, color, horn, wings);
	std::cout << "My name is " << StackPony.getName() <<", my color is " << StackPony.getColor()  <<
	". horn? " << StackPony.getHorn() << ". wings? " << StackPony.getWings() << std::endl;
}

int main()
{
	std::cout << "Hello. input pony name, color, it have a horn and wings?: " << std::endl;
	std::string name, color, horn, wings;

	std::cin >>  name >> color >> horn >> wings;
	ponyOnTheStack(name, color, horn, wings);
	ponyOnTheHeap(name, color, horn, wings);
	return 0;
}
