//
// Created by Oleh SAK on 2019-10-02.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>

void replaceAll(std::string &str, const char *find, const char *replace) {
	size_t iter = 0;
	size_t length = strlen(replace);

	while ((iter = str.find(find, iter)) != std::string::npos)
	{
		str.replace(iter, length, replace);
		iter += length;
	}
}

int main(int argc, char **argv)
{
	if (argc == 4)
	{
		std::ifstream file(argv[1]);

		if (file.is_open())
		{
			std::string line;

			line = argv[1];
			line += ".replace";

			std::ofstream ofile(line.c_str());
			line.clear();

			getline(file, line, (char)EOF);
			file.close();
			replaceAll(line, argv[2], argv[3]);
			ofile << line;
		}
		else
			std::cout << "File open error, maybe file not found" << std::endl;
	}
	else
		std::cout << "Wrong count parameters" << std::endl;
	return (0);
}