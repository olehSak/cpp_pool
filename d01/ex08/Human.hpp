//
// Created by Oleh SAK on 2019-10-02.
//

#ifndef CPP_POOL_HUMAN_H
#define CPP_POOL_HUMAN_H

#include <iostream>
#include <string>



class Human
{
	typedef void (Human::*func)(std::string const &);

private:
	std::string *_action_names;
	func 		*_actions;
	size_t      _actions_count;
	std::string _name;

private:
	void		meleeAttack(std::string const &target);
	void		rangedAttack(std::string const &target);
	void		intimidatingShout(std::string const &target);

public:
	Human(const char *name);
	~Human();
	void		action(std::string const &action_name, std::string const &target);
};

#endif //CPP_POOL_HUMAN_HPP
