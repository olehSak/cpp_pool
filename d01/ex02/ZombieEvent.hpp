#ifndef ZOMBIEEVENT_HPP
#define ZOMBIEEVENT_HPP

class ZombieEvent {
public:
	ZombieEvent();
	~ZombieEvent();

	void			setZombieType(std::string data);
	Zombie			*newZombie(std :: string name);
private:
	std::string		type;
};

#endif
