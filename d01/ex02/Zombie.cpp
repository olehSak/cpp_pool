// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Zombie.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/10/01 17:58:27 by osak              #+#    #+#             //
//   Updated: 2019/10/01 17:58:29 by osak             ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Zombie.hpp"
#include "ZombieEvent.cpp"

Zombie::Zombie(std::string name, std::string type)
{
	setName(name);
	setType(type);
}

Zombie::~Zombie()
{
	std::cout << "Destructor called" << std::endl;
}

int 	Zombie::getRand(int i)
{
	return (1 + std::rand() % i);
}

void	Zombie::announce()
{
	std::cout << "<" + this->getName() << " (" << this->getType() << ")> " << getRand(5) << "Braiinzzzzzz..." << std::endl;
}

void		Zombie::setName(std::string data)
{
	this->_name = data;
}

void 		Zombie::setType(std::string data)
{
	this->_type = data;
}

std::string	Zombie::getName(void)
{
	return this->_name;
}

std::string	Zombie::getType(void)
{
	return this->_type;
}
