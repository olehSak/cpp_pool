#include "ZombieEvent.hpp"

ZombieEvent::ZombieEvent()
{
	std::cout << "Constructor called" << std::endl;
}

ZombieEvent::~ZombieEvent()
{
	std::cout << "Destructor called" << std::endl;
	return;
}

void		ZombieEvent::setZombieType(std::string type)
{
	this->type = type;
}

Zombie *ZombieEvent::newZombie(std::string name)
{
	Zombie *newZombie = new Zombie(name, this->type);
	return newZombie;
}