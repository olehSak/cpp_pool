#include "Zombie.cpp"

void	randomChump(ZombieEvent *random)
{
	std::string		name[5];
	int i = Zombie::getRand(5);
	name[0] = "Mary";
	name[1] = "Jane";
	name[2] = "John";
	name[3] = "Dimoooon";
	name[4] = "Hulk";
	random->setZombieType("Angry");
	Zombie *okurwa = random->newZombie(name[i]);
	okurwa->announce();
}

int main()
{
	std::cout << "Heap Event:" << std::endl;
	ZombieEvent *HeapEvent = new ZombieEvent;
	randomChump(HeapEvent);
	delete HeapEvent;

	std::cout << "Stack Event:" << std::endl;
	ZombieEvent StackEvent;
	StackEvent.setZombieType("Bastard");
	Zombie 		*stackZombie = StackEvent.newZombie("0kurwa");
	stackZombie->announce();
	return 0;
}