// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ex01.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2019/10/01 17:56:33 by osak              #+#    #+#             //
//   Updated: 2019/10/01 17:56:36 by osak             ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

void	memoryLeak()
{
  std::string	*panthere = new std::string("String panthere");
  std::cout << *panthere << std::endl;
  delete panthere;
}
