#include "span.hpp"

Span::Span(unsigned int size):
		_datas(size), _datasv(size+1)
{
	_data_count = 0;
}

Span::Span(Span const &src)
{
	*this = src;
}

Span::~Span()
{
}

Span				&Span::operator=(Span const &rhs)
{
	_data_count = rhs._data_count;
	_datas = rhs._datas;
	return (*this);
}

void				Span::addNumber(int number) throw(SpanFullException)
{
		if (_data_count >= _datas.size())
			throw (SpanFullException());
	_datas[_data_count] = number;
	_data_count++;
}

void				Span::addNumber(std::vector<int>::iterator it, std::vector<int>::iterator end) throw(SpanFullException)
{
		if (_data_count + std::distance(it, end) >=
			static_cast<int>(_datas.size()))
			throw (SpanFullException());
		while (it != end)
		{
			_datas[_data_count] = *it;
			++_data_count;
			++it;
		}
}

int					Span::shortestSpan()
{
	_datasc = _datas;
	std::vector<int>::iterator	it = _datasc.begin();
	std::vector<int>::iterator	iv = _datasv.begin();
	std::sort(it, it + _data_count);
	std::adjacent_difference(it, it + _data_count, iv);
	return (*std::min_element(iv + 1, iv + _data_count));
	}

int					Span::longestSpan()
{
	std::vector<int>::iterator	it = _datas.begin();
	return (*std::max_element(it, it + _data_count) - *std::min_element(it, it + _data_count));
}

Span::SpanFullException::SpanFullException() {
}
Span::SpanFullException::~SpanFullException() throw() {}

char const			*Span::SpanFullException::what() const throw()
{
	return ("Span is full");
}
