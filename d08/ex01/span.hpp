#ifndef CPP_POOL_SPAN_HPP
#define CPP_POOL_SPAN_HPP

#include <vector>
#include <exception>
#include <iostream>
#include <numeric>

class Span
{
private:
	std::vector<int>		_datas;
	std::vector<int>		_datasc;
	std::vector<int>		_datasv;

	unsigned int			_data_count;

public:
	class					SpanFullException: public std::exception
	{
	public:
		SpanFullException();
		~SpanFullException() throw();
		char const				*what() const throw();
	};

	Span(unsigned int size);
	~Span();
	Span(Span const &src);
	Span					&operator=(Span const &rhs);

	void					addNumber(int number) throw(SpanFullException);
	void					addNumber(std::vector<int>::iterator it, std::vector<int>::iterator end) throw(SpanFullException);

	int						shortestSpan();
	int						longestSpan();
};


#endif
