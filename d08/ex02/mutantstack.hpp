#ifndef MUTANTSTACK_H
# define MUTANTSTACK_H

#include <iostream>
#include <stack>

template <class T>
class MutantStack : public std::stack<T>
{

public:
	typedef typename std::stack<T>::container_type::iterator iterator;
	MutantStack() {}
	MutantStack(MutantStack const & src) : std::stack<T>(src){ }
	~MutantStack() { }
	using 	std::stack<T>::operator=;

	iterator	begin() { return std::stack<T>::c.begin(); }
	iterator	end() { return std::stack<T>::c.end(); }
};

#endif
