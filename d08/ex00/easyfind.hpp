#ifndef CPP_POOL_EASYFIND_HPP
#define CPP_POOL_EASYFIND_HPP

#include <ostream>

template<typename T>

typename T::iterator easyfind(T &container, int data)
{
	typename T::iterator it_end = container.end();

	for(typename T::iterator it = container.begin(); it != it_end; it++)
	{
		if (*it== data)
			return (it);
	}
	return (it_end);
}

#endif //CPP_POOL_EASYFIND_HPP
