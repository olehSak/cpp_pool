#include "easyfind.hpp"

#include <list>
#include <string>
#include <iostream>

#include "math.h"

int 	main()
{
	std::list<int>::iterator	result;
	std::list<int>				lst;

	lst.push_back(2);
	lst.push_back(4);
	lst.push_back(8);
	lst.push_back(16);
	lst.push_back(21);
	lst.push_back(64);

	try {
		result = easyfind(lst, 64);
		if (result == lst.end())
			throw "Not found";
		std::cout << "Search value: " << *result << std::endl;

	}
	catch (const char* exception) // обработчик исключений типа const char*
	{
		std::cerr << "Error: " << exception << std::endl;
	}

	return 0;
}