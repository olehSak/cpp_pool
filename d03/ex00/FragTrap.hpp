#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP

#include <string>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <ctime>

class FragTrap {
private:
	static unsigned int	_serialNumber;

	unsigned int _hitPoints;
	unsigned int _maxHitPoints;
	unsigned int _energyPoints;
	unsigned int _maxEnergyPoints;
	unsigned int _level;
	std::string	 _name;
	unsigned int _meleeAttackDamage;
	unsigned int _rangedAttackDamage;
	unsigned int _armorDamageReduction;

public:
	FragTrap(void);
	FragTrap(std::string name);
	FragTrap(FragTrap const & src);
	~FragTrap();

	unsigned int rangedAttack(std::string const & target);
	unsigned int meleeAttack(std::string const & target);
	unsigned int laserInfernoAttack(std::string const &target);
	unsigned int torgueFiestaAttack(std::string const &target);
	unsigned int pirateShipModeAttack(std::string const &target);
	unsigned int vaulthunter_dot_exe(std::string const & target);

	void		 takeDamage(unsigned int amount);
	void		 beRepaired(unsigned int amount);

	FragTrap &			operator=(FragTrap const & rhs);
};


#endif
