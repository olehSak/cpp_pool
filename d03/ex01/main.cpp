#include "FragTrap.cpp"
#include "ScavTrap.cpp"

int main(void)
{
	ScavTrap fTrap("B4R-BOT");

	fTrap.takeDamage(10);
	fTrap.challengeNewcomer("1337");
	fTrap.challengeNewcomer("1337");
	fTrap.challengeNewcomer("1337");
	fTrap.challengeNewcomer("1337");
	fTrap.challengeNewcomer("1337");
	fTrap.challengeNewcomer("1337");
	return 0;
}

