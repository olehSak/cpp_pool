#include "ScavTrap.hpp"

unsigned int 	ScavTrap::_serialNumber = 1;

int				ScavTrap::_challengeNumber = 4;
std::string		ScavTrap::_challenges[] = {
		"Who was in vault",
		"How mach ammo in My GUNN",
		"Make a robo party",
		"What is the Best Weapon Brand"
};

ScavTrap::ScavTrap(void): _hitPoints(100),
						  _maxHitPoints(100),
						  _maxEnergyPoints(50),
						  _energyPoints(50),
						  _level(1),
						  _meleeAttackDamage(20),
						  _rangedAttackDamage(15),
						  _armorDamageReduction(3){
	std::srand(time(0));
	std::ostringstream ostr;
	ostr << this->_serialNumber;
	this->_name += "#";
	this->_name += ostr.str();
	std::cout << this->_name << ": ScavTrap Hey everybody! Check out my package!" << std::endl;
}

ScavTrap::ScavTrap(std::string name): _hitPoints(100),
									  _maxHitPoints(100),
									  _maxEnergyPoints(50),
									  _energyPoints(50),
									  _level(1),
									  _meleeAttackDamage(20),
									  _rangedAttackDamage(15),
									  _armorDamageReduction(3){
	std::srand(time(0));
	this->_name = name;
	std::cout << this->_name << ": ScavTrap Hey everybody! Check out my package!" << std::endl;
}

ScavTrap::ScavTrap(ScavTrap const &src)
{
	std::srand(time(0));
	*this = src;
	std::cout << "ScavTrap: DUPLICATEEEEEED"<< std:: endl;
}

ScavTrap		&ScavTrap::operator=(ScavTrap const	&rhs)
{
	if (this != &rhs)
	{
		this->_name = rhs._name;
		this->_hitPoints = rhs._hitPoints;
		this->_energyPoints = rhs._energyPoints;
		this->_level = rhs._level;
	}
	return (*this);
}

ScavTrap::~ScavTrap() {
	std::cout << this->_name << ": ScavTrap The robot is dead, long live the robot!" << std::endl;
}

void			ScavTrap::takeDamage(unsigned int amount){
	signed long long int calcAmount = amount;
	calcAmount -= _armorDamageReduction;

	if (this->_hitPoints < 1) {
		std::cout << this->_name << ": I\'m too pretty to die!" << std::endl;
		return ;
	}

	if (calcAmount < 1)
		std::cout << this->_name << ": What\'s that smell? Oh wait, it's just you!" << std::endl;
	else {
		if (this->_hitPoints - calcAmount < 0) {
			std::cout << this->_name << ": I\'m too pretty to die!" << std::endl;
			_hitPoints = 0;
		}
		else {
			std::cout << this->_name << ": Extra ouch!" << std::endl;
			this->_hitPoints -= calcAmount;
		}
	}
}

void			ScavTrap::beRepaired(unsigned int amount) {
	this->_hitPoints += amount;

	if (_hitPoints > _maxHitPoints) {
		std::cout << this->_name << ": Health over here!" << std::endl;
		_hitPoints = _maxEnergyPoints;
	}
	else
		std::cout << this->_name << ": Sweet life juice!" << std::endl;

	this->_energyPoints += amount;

	if (_energyPoints > _maxEnergyPoints) {
		std::cout << this->_name << ": Health over here!" << std::endl;
		_energyPoints = _maxEnergyPoints;
	}
	else
		std::cout << this->_name << ": Sweet life juice!" << std::endl;
}

unsigned int	ScavTrap::meleeAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  << " at melee, causing " <<_meleeAttackDamage << " points of damage !" << std::endl;
	return (_meleeAttackDamage);
}

unsigned int	ScavTrap::rangedAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  << " at range, causing " <<_rangedAttackDamage << " points of damage !" << std::endl;
	return (_rangedAttackDamage);
}

unsigned int	ScavTrap::laserInfernoAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  <<  " with laser Inferno, causing " << 40 << " points of damage !" << std::endl;
	return (_rangedAttackDamage);
}

unsigned int	ScavTrap::torgueFiestaAttack(std::string const &target){
	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  <<  " with torgue Fiesta, causing " << 32 << " points of damage !" << std::endl;
	return (32);
}

unsigned int	ScavTrap::pirateShipModeAttack(std::string const &target){
	int damage = _maxHitPoints + _energyPoints * (_level / 100);

	std::cout << "FR4G-TP "  << this->_name <<  " attacks " << target
			  <<  " with Pirate Ship Mode, causing " << damage << " points of damage !" << std::endl;
	return (damage);
}

void			ScavTrap::challengeNewcomer(std::string const &target)
{
	int			random;

	random = rand() % _challengeNumber;
	std::cout << this->_name << ": " << _challenges[random] << " " << target << std::endl;
}