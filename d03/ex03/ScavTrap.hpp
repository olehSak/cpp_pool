#ifndef SCAVTRAP_HPP
#define SCAVTRAP_HPP

#include "ClapTrap.hpp"

class ScavTrap: public ClapTrap {

private:
	static int			_challengeNumber;
	static std::string	_challenges[];

public:
	ScavTrap(void);
	ScavTrap(std::string name);
	~ScavTrap();

	ScavTrap(ScavTrap const &src);

	ScavTrap			&operator=(ScavTrap const &rhs);

	void				challengeNewcomer(std::string const &target);
};


#endif
