#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap(void): ClapTrap(){
	_hitPoints = 60;
	_maxHitPoints = 60;
	_energyPoints = 120;
	_maxEnergyPoints = 120;
	_meleeAttackDamage = 60;
	_rangedAttackDamage = 5;
	_armorDamageReduction = 0;
	std::cout << this->_name << ": NinjaTrap Recompiling my combat code!" << std::endl;
}

NinjaTrap::NinjaTrap(std::string name): ClapTrap(name){
	_hitPoints = 60;
	_maxHitPoints = 60;
	_energyPoints = 120;
	_maxEnergyPoints = 120;
	_meleeAttackDamage = 60;
	_rangedAttackDamage = 5;
	_armorDamageReduction = 0;
	std::cout << this->_name << ": NinjaTrap Recompiling my combat code!" << std::endl;
}

NinjaTrap::~NinjaTrap(){
	std::cout << this->_name << ": I\'M A NinjaTrap I\'M DEAD OHMYGOD I\'M DEAD!" << std::endl;
}

NinjaTrap::NinjaTrap(NinjaTrap const &src):
		ClapTrap(src._name, 60, 120, 60, 5, 0)
{
	std::cout << this->_name << ": NinjaTrap Hey everybody! Check out my package! DUPLICATEEEEED" << std::endl;
}

NinjaTrap		&NinjaTrap::operator=(NinjaTrap const	&rhs)
{
	if (this != &rhs)
		ClapTrap::operator=(rhs);
	return *this;
}

void			NinjaTrap::ninjaShoebox(ClapTrap &target) const
{
	std::cout << _name << " It was is with just ClapTrap" << std::endl;
	target.takeDamage(rand() % 50);
}

void			NinjaTrap::ninjaShoebox(FragTrap &target) const
{
	std::cout << _name << " Super effective on FragTrap!" << std::endl;
	target.takeDamage(rand() % 50);
}

void			NinjaTrap::ninjaShoebox(ScavTrap &target) const
{
	std::cout << _name << " Not so effective on ScavTrap!" << std::endl;
	target.takeDamage(rand() % 10);
}

void			NinjaTrap::ninjaShoebox(NinjaTrap &target) const
{
	std::cout << _name << " Cant effective fight with NinjaTrap!" << std::endl;
	target.takeDamage(rand() % 4);
}