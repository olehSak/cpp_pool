#ifndef NINJATRAP_HPP
#define NINJATRAP_HPP

#include "ClapTrap.hpp"
#include "FragTrap.hpp"
#include "ScavTrap.hpp"

class NinjaTrap : public ClapTrap{
public:
	NinjaTrap(void);
	NinjaTrap(std::string name);
	~NinjaTrap();
	NinjaTrap(NinjaTrap const &src);

	NinjaTrap		&operator=(NinjaTrap const &rhs);

	void			ninjaShoebox(ClapTrap &target) const;
	void			ninjaShoebox(FragTrap &target) const;
	void			ninjaShoebox(ScavTrap &target) const;
	void			ninjaShoebox(NinjaTrap &target) const;
};


#endif
