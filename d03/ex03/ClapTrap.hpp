#ifndef CLAPTRAP_HPP
#define CLAPTRAP_HPP

#include <string>
#include <sstream>
#include <iostream>
#include <cstdlib>
#include <ctime>

class ClapTrap {
public:
	ClapTrap(void);
	ClapTrap(std::string name);
	ClapTrap(std::string name, int hp, int en, int mad, int rad, int adr);
	ClapTrap(ClapTrap const &src);
	~ClapTrap();

	ClapTrap			&operator=(ClapTrap const &rhs);

	unsigned int		meleeAttack(std::string const &target);
	unsigned int		rangedAttack(std::string const &target);
	unsigned int		laserInfernoAttack(std::string const &target);
	unsigned int		torgueFiestaAttack(std::string const &target);
	unsigned int		pirateShipModeAttack(std::string const &target);
	void				takeDamage(unsigned int amount);
	void				beRepaired(unsigned int amount);

protected:
	static unsigned int	_serialNumber;

	unsigned int		_hitPoints;
	unsigned int 		_maxHitPoints;
	unsigned int		_level;
	unsigned int		_meleeAttackDamage;
	unsigned int		_rangedAttackDamage;
	unsigned int		_armorDamageReduction;
	unsigned int		_energyPoints;
	unsigned int 		_maxEnergyPoints;
	std::string			_name;
};


#endif
