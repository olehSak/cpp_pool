#ifndef FRAGTRAP_HPP
#define FRAGTRAP_HPP

#include "ClapTrap.hpp"

class FragTrap: public ClapTrap {
public:
	FragTrap(void);
	FragTrap(std::string name);
	~FragTrap();
	FragTrap(FragTrap const &src);

	FragTrap			&operator=(FragTrap const &rhs);

	unsigned int		vaulthunter_dot_exe(std::string const & target);
};


#endif
