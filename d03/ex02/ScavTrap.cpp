#include "ScavTrap.hpp"


int				ScavTrap::_challengeNumber = 4;
std::string		ScavTrap::_challenges[] = {
		"Who was in vault",
		"How mach ammo in My GUNN",
		"Make a robo party",
		"What is the Best Weapon Brand"
};

ScavTrap::ScavTrap(void): ClapTrap(){

	std::cout << this->_name << ": ScavTrap Hey everybody! Check out my package!" << std::endl;
}

ScavTrap::ScavTrap(ScavTrap const &src):
		ClapTrap(src._name, 100, 50, 20, 15, 3)
{
	std::cout << this->_name << ": ScavTrap Hey everybody! Check out my package! DUPLICATEEEEED" << std::endl;
}

ScavTrap		&ScavTrap::operator=(ScavTrap const	&rhs)
{
	if (this != &rhs)
		ClapTrap::operator=(rhs);
	return *this;
}

ScavTrap::ScavTrap(std::string name): ClapTrap(){
	_energyPoints = 50;
	_maxEnergyPoints = 50;
	this->_name = name;
	std::cout << this->_name << ": ScavTrap Hey everybody! Check out my package!" << std::endl;
}

ScavTrap::~ScavTrap() {
	std::cout << this->_name << ": ScavTrap The robot is dead, long live the robot!" << std::endl;
}

void			ScavTrap::challengeNewcomer(std::string const &target)
{
	int			random;
	random = rand() % _challengeNumber;
	std::cout << this->_name << ": " << _challenges[random] << " " << target << std::endl;
}