#include "FragTrap.hpp"

FragTrap::FragTrap(void): ClapTrap(){
	std::cout << this->_name << ": FragTrap Recompiling my combat code!" << std::endl;
}

FragTrap::FragTrap(std::string name): ClapTrap(name){
	std::cout << this->_name << ": FragTrap Recompiling my combat code!" << std::endl;
}

FragTrap::FragTrap(FragTrap const &src):
		ClapTrap(src._name, 100, 100, 30, 20, 5)
{
	std::cout << "FragTrap: Duplicated" << std::endl;
}

FragTrap::~FragTrap() {
	std::cout << this->_name << ": FragTrap I\'M DEAD I\'M DEAD OH MY GOD I\'M DEAD!" << std::endl;
}

FragTrap		&FragTrap::operator=(FragTrap const	&rhs)
{
	if (this != &rhs)
		ClapTrap::operator=(rhs);
	return *this;
}


unsigned int	FragTrap::vaulthunter_dot_exe(std::string const &target){
	if (_energyPoints < 25) {
		std::cout << "No energy to do this"<< std::endl;
		return (0);
	}

	_energyPoints -=25;

	int rndNumber = std::rand() % 5;

	std::cout << rndNumber;
	if (rndNumber == 0)
		return meleeAttack(target);
	if (rndNumber == 1)
		return rangedAttack(target);
	if (rndNumber == 2)
		return laserInfernoAttack(target);
	if (rndNumber == 3)
		return torgueFiestaAttack(target);
	//if (rndNumber == 4)
	return pirateShipModeAttack(target);
}
