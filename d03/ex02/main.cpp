#include "FragTrap.cpp"
#include "ScavTrap.cpp"
#include "ClapTrap.cpp"

int main(void)
{

	ClapTrap cTrap("CLAP-9000e");
	cTrap.takeDamage(10);

	ScavTrap sTrap("SC4V-TP");

	sTrap.takeDamage(10);
	sTrap.challengeNewcomer("1337");

	FragTrap fTrap("B4R-BOT");

	fTrap.vaulthunter_dot_exe("CU5TM-TP");
	fTrap.vaulthunter_dot_exe("CU5TM-TP");
	fTrap.vaulthunter_dot_exe("CU5TM-TP");
	fTrap.vaulthunter_dot_exe("CU5TM-TP");
	fTrap.vaulthunter_dot_exe("CU5TM-TP");
	fTrap.vaulthunter_dot_exe("CU5TM-TP");
	fTrap.vaulthunter_dot_exe("CU5TM-TP");
	return 0;
}

