#ifndef ROBOTOMYREQUESTFORM_HPP
#define ROBOTOMYREQUESTFORM_HPP

#include <iostream>
#include <string>
#include <time.h>
#include <stdlib.h>
#include "Form.hpp"
#include "RobotomyRequestForm.hpp"

class RobotomyRequestForm: public Form
{
	private:
		std::string				_target;

	public:
								RobotomyRequestForm(std::string const &target);
								~RobotomyRequestForm();
								RobotomyRequestForm(RobotomyRequestForm const &src);
		RobotomyRequestForm		&operator=(RobotomyRequestForm const &rhs);

		void					beExecuted() const;

		std::string	const		&getTarget() const;
};

#endif
