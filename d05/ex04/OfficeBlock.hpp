
#ifndef OFFICEBLOCK_HPP
#define OFFICEBLOCK_HPP

#include "Intern.hpp"
#include "Bureaucrat.hpp"

class OfficeBlock {
	private:
		Intern					*_intern;
		Bureaucrat				*_signer;
		Bureaucrat				*_executor;

	public:
		class					NoInternException: public std::exception
		{
			public:
				NoInternException();
				~NoInternException() throw();
				NoInternException(NoInternException const &src);
				NoInternException		&operator=(NoInternException const &rhs);
				char const				*what() const throw();
		};

		class					NoSignerException: public std::exception
		{
			public:
				NoSignerException();
				~NoSignerException() throw();
				NoSignerException(NoSignerException const &src);
				NoSignerException		&operator=(NoSignerException const &rhs);
				char const				*what() const throw();
		};

		class					NoExecutorException: public std::exception
		{
			public:
				NoExecutorException();
				~NoExecutorException() throw();
				NoExecutorException(NoExecutorException const &src);
				NoExecutorException		&operator=(NoExecutorException const &rhs);
				char const				*what() const throw();
		};

		class					SignerGradeTooLowException: public std::exception
		{
			public:
				SignerGradeTooLowException();
				~SignerGradeTooLowException() throw();
				SignerGradeTooLowException(SignerGradeTooLowException const &src);
				SignerGradeTooLowException		&operator=(SignerGradeTooLowException const &rhs);
				char const				*what() const throw();
		};

		class					ExecutorGradeTooLowException: public std::exception
		{
			public:
				ExecutorGradeTooLowException();
				~ExecutorGradeTooLowException() throw();
				ExecutorGradeTooLowException(ExecutorGradeTooLowException const &src);
				ExecutorGradeTooLowException		&operator=(ExecutorGradeTooLowException const &rhs);
				char const				*what() const throw();
		};

		OfficeBlock();
		~OfficeBlock();
		OfficeBlock(Intern &intern, Bureaucrat &signer, Bureaucrat &executor);

		void					doBureaucracy(std::string const &name, std::string const &target);

		void					setIntern(Intern &intern);
		void					setSigner(Bureaucrat &signer);
		void					setExecutor(Bureaucrat &executor);
};


#endif 
