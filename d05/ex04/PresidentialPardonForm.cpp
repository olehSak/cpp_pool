#include "PresidentialPardonForm.hpp"

PresidentialPardonForm::PresidentialPardonForm(std::string const &target):
	Form("Presidential Pardon Form", 25, 5),
	_target(target)
{
	srand(time(NULL));
}

PresidentialPardonForm::PresidentialPardonForm(PresidentialPardonForm const &src):
	Form("Presidential Pardon Form", 25, 5)
{
	*this = src;
}

PresidentialPardonForm::~PresidentialPardonForm()
{}

PresidentialPardonForm		&PresidentialPardonForm::operator=(PresidentialPardonForm const &rhs)
{
	this->_target = rhs._target;
	return (*this);
}

void						PresidentialPardonForm::beExecuted() const
{
		std::cout << this->_target << " has been pardoned by Zafod Beeblebrox" << std::endl;
}

std::string const			&PresidentialPardonForm::getTarget() const {
	return (this->_target);
}
