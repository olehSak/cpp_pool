#include "Intern.hpp"

Intern::Intern()
{}

Intern::Intern(Intern const &src)
{
	*this = src;
}

Intern::~Intern()
{}

Intern				&Intern::operator=(Intern const &rhs)
{
	(void)rhs;
	return (*this);
}

Form				*Intern::makeForm(std::string const &name, std::string const &target)
{
	Form			*form;

	if (name == "shrubbery creation")
		form = (Form *)new ShrubberyCreationForm(target);
	else if (name == "robotomy request")
		form = (Form *)new RobotomyRequestForm(target);
	else if (name == "presidential pardon")
		form = (Form *)new PresidentialPardonForm(target);
	else
	{
		std::cout << "Intern can't find the " << name << " form !" << std::endl;
		return (NULL);
	}
	std::cout << "Intern creates " << *form;
	return (form);
}