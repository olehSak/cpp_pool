#include "ShrubberyCreationForm.hpp"

ShrubberyCreationForm::ShrubberyCreationForm(std::string const &target):
	Form("Shrubbery Creation Form", 145, 137),
	_target(target)
{}

ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const &src):
	Form("Shrubbery Creation Form", 145, 137)
{
	*this = src;
}

ShrubberyCreationForm::~ShrubberyCreationForm()
{}

ShrubberyCreationForm		&ShrubberyCreationForm::operator=(ShrubberyCreationForm const &rhs)
{
	this->_target = rhs._target;
	return (*this);
}

void						ShrubberyCreationForm::beExecuted() const
{
	std::ofstream			file;

	file.open(this->_target + "_shrubbery");
	if (file.is_open())
	{
		file << "---------------" << std::endl
			 << "|   /  \\      |" << std::endl
			 << "|    /\\       |" << std::endl
			 << "|   /  \\      |" << std::endl
			 << "|    ||       |" << std::endl
			 << "|       /\\    |" << std::endl
			 << "|       /\\    |" << std::endl
			 << "|       ||    |" << std::endl
			 << "---------------" << std::endl;
	}
}

std::string const			&ShrubberyCreationForm::getTarget() const {
	return (this->_target);
}
