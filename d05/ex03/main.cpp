#include "Bureaucrat.hpp"
#include "Form.hpp"
#include "ShrubberyCreationForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "PresidentialPardonForm.hpp"
#include "Intern.hpp"

int				main()
{
	Intern					intern1;

	intern1.makeForm("go home", "just here");
	Form					*form1 = intern1.makeForm("shrubbery creation", "home");
	Form					*form2 = intern1.makeForm("robotomy request", "school");
	Form					*form3 = intern1.makeForm("presidential pardon", "president");
	std::cout << *form1;
	std::cout << *form2;
	std::cout << *form3;

	try
	{
		Bureaucrat	b1("b1", 150);
		std::cout << b1;
		b1.signForm(*form1);
		b1.executeForm(*form1);
		std::cout << *form1;
	}
	catch (std::exception &e)
	{
		std::cout << "ERROR b1 " << e.what() << std::endl;
	}

	try
	{
		Bureaucrat	b2("b2", 140);
		std::cout << b2;
		b2.signForm(*form1);
		b2.executeForm(*form1);
		std::cout << *form1;
	}
	catch (std::exception &e)
	{
		std::cout << "ERROR b2 " << e.what() << std::endl;
	}

	try
	{
		Bureaucrat	b3("b3", 2);
		std::cout << b3;
		b3.executeForm(*form1);
		std::cout << *form1;

		b3.signForm(*form2);
		b3.executeForm(*form2);
		std::cout << *form2;

		b3.signForm(*form3);
		b3.executeForm(*form3);
		std::cout << *form3;
	}
	catch (std::exception &e)
	{
		std::cout << "ERROR b3" << e.what() << std::endl;
	}

	return (0);
}
