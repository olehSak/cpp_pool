#include "Bureaucrat.hpp"
#include "Form.hpp"

int		main()
{
	Form		form("Form 42C", 21, 21);
	std::cout << form;

	try
	{
		Bureaucrat	b1("b1", 42);
		std::cout << b1;
		b1.signForm(form);
		std::cout << form;
	}
	catch (std::exception &e)
	{
		std::cout << "ERROR b1 " << e.what() << std::endl;
	}

	try
	{
		Bureaucrat	b2("b2", 2);
		std::cout << b2;
		b2.signForm(form);
		std::cout << form;
	}
	catch (std::exception &e)
	{
		std::cout << "ERROR b2 " << e.what() << std::endl;
	}
}
