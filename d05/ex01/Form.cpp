#include "Form.hpp"

Form::Form(std::string const &name, int minGradeSign, int minGradeExec) throw(GradeTooHighException, GradeTooLowException): _name(name)
{
	if (minGradeSign < 1 || minGradeExec < 1)
		throw (GradeTooHighException());
	if (minGradeSign > 150 || minGradeExec > 150)
		throw (GradeTooLowException());
	this->_minSignGrade = minGradeSign;
	this->_minExecGrade = minGradeExec;
}

Form::Form(Form const &src)
{
	*this = src;
}

Form::~Form() {}

Form					&Form::operator=(Form const &rhs)
{
	this->_name = rhs._name;
	this->_minSignGrade = rhs._minSignGrade;
	this->_minExecGrade = rhs._minExecGrade;
	return (*this);
}

void						Form::beSigned(Bureaucrat const &bureaucrat) throw(GradeTooLowException)
{
	if (bureaucrat.getGrade() > this->_minSignGrade)
		throw (GradeTooLowException());
	this->_signed = true;
}

bool						Form::isSigned() const {
	return (this->_signed);
}

std::string	const			&Form::getName() const {
	return (this->_name);
}

int							Form::getMinSignGrade() const {
	return (this->_minSignGrade);
}

int							Form::getMinExecGrade() const {
	return (this->_minExecGrade);
}

std::ostream				&operator<<(std::ostream &os, Form const &form)
{
	os	<< form.getName() << ", ";
	if (!form.isSigned())
		os << "not ";
	os	<< "signed, min sign grade : " << form.getMinSignGrade()
		  << ", min execution grade : " << form.getMinExecGrade() << std::endl;
	return (os);
}

Form::GradeTooHighException::GradeTooHighException() {}
Form::GradeTooHighException::~GradeTooHighException() throw() {}
Form::GradeTooHighException::GradeTooHighException(GradeTooHighException const &src)
{
	*this = src;
}

Form::GradeTooHighException	&Form::GradeTooHighException::operator=(Form::GradeTooHighException const &rhs)
{
	std::exception::operator=(rhs);
	return (*this);
}

char const					*Form::GradeTooHighException::what() const throw()
{
	return ("Grade Too High");
}

Form::GradeTooLowException::GradeTooLowException() {

}

Form::GradeTooLowException::~GradeTooLowException() throw() {

}

Form::GradeTooLowException::GradeTooLowException(GradeTooLowException const &src)
{
	*this = src;
}

Form::GradeTooLowException	&Form::GradeTooLowException::operator=(Form::GradeTooLowException const &rhs)
{
	std::exception::operator=(rhs);
	return (*this);
}

char const					*Form::GradeTooLowException::what() const throw()
{
	return ("Grade Too Low");
}
