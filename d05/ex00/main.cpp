#include "Bureaucrat.hpp"

int		main()
{
	try
	{
		Bureaucrat	b1("b1", 2);
		std::cout << b1 << std::endl;;
		b1.incrementGrade();
		std::cout << b1 << std::endl;;
		b1.incrementGrade();
		std::cout << b1 << std::endl;;
	}
	catch (std::exception &e)
	{
		std::cout << "ERROR b1 "<< e.what() << std::endl;
	}

	try
	{
		Bureaucrat	b2("b2", 0);
	}
	catch (Bureaucrat::GradeTooHighException &e)
	{
		std::cout << "ERROR b2 " << e.what() << std::endl;
	}

	try
	{
		Bureaucrat	b3("b3", 149);
		std::cout << b3 << std::endl;
		b3.decrementGrade();
		std::cout << b3 << std::endl;
		b3.decrementGrade();
		std::cout << b3 << std::endl;
	}
	catch (Bureaucrat::GradeTooLowException &e)
	{
		std::cout << "ERROR b3 " << e.what() << std::endl;
	}
}
