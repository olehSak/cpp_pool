#ifndef CPP_POOL_BUREAUCRAT_HPP
#define CPP_POOL_BUREAUCRAT_HPP

#include <string>
#include <iostream>
# include <exception>

class	Bureaucrat {
private:
	std::string		_name;
	int 			_grade;
public:
	class	GradeTooHighException: public std::exception
	{
	public:
		GradeTooHighException();
		~GradeTooHighException() throw();
		GradeTooHighException(GradeTooHighException const &src);
		GradeTooHighException	&operator=(GradeTooHighException const &rhs);

		char const				*what() const throw();
	};

	class	GradeTooLowException: public std::exception
	{
	public:
		GradeTooLowException();
		~GradeTooLowException() throw();
		GradeTooLowException(GradeTooLowException const &src);
		GradeTooLowException	&operator=(GradeTooLowException const &rhs);

		char const		*what() const throw();
	};
	Bureaucrat(std::string name, int grade);
	~Bureaucrat();
	Bureaucrat(Bureaucrat const & src);

	Bureaucrat &		operator=(Bureaucrat const & rhs);
	std::string const 	&getName() const;
	int const 			&getGrade() const;
	void 				incrementGrade();
	void				decrementGrade();
};

std::ostream	&operator<<(std::ostream &out, Bureaucrat const &rhs);

#endif //CPP_POOL_BUREAUCRAT_HPP
