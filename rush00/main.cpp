/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apluzhni <apluzhni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/06 13:52:01 by apluzhni          #+#    #+#             */
/*   Updated: 2019/10/06 18:28:52 by apluzhni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <string>
#include <ncurses.h>
#include <sys/time.h>

#include "Drawer.cpp"
#include "Character.cpp"

int		main(void)
{
	struct timeval stop, start;
	gettimeofday(&start, NULL);
	initscr();
	Drawer		game;
	curs_set(0);
	game.loadScreen();
	while (game.run)
	{
		gettimeofday(&stop, NULL);
		if ((unsigned long)stop.tv_usec - (unsigned long)start.tv_usec > 100000)
		{
			game.events();
			start = stop;
		}
	}
	endwin();
	return 0;
}
