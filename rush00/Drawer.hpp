/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Drawer.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apluzhni <apluzhni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/06 13:52:37 by apluzhni          #+#    #+#             */
/*   Updated: 2019/10/06 18:45:11 by apluzhni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DRAWER_HPP
# define DRAWER_HPP

# include <ctime>
# include <ncurses.h>
# include <curses.h>

# include "Character.hpp"

# define WIN_H	35
# define WIN_W	90

class Drawer
{
	public:

		Drawer(void);
		~Drawer(void);
		Drawer(Drawer const &src);

		Drawer &	operator=(Drawer const & rhs);


		void		loadScreen(void);
		void		events(void);
		void		fillBuff(void);
		void		drawBuff(void);
		void		cleanPlayer(void);
		void		drawPlayer(void);
		int			getRand(int limit);
		void		moveBack(void);
		void 		generateRow(void);
		bool		run;

	private:

		WINDOW		*nWindow;
		char		**buff;
		Character	player;
};

#endif
