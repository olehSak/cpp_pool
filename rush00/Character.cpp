//
// Created by Oleh SAK on 2019-10-06.
//

#include "Character.hpp"


Character::Character(void) : _y(1), _HP(3), _score(0), _ammo(10)
{
	_charact = new char* [3];

	_charact[1] = new char [4];
	_charact[1][1] = '@';
	_charact[1][2] = '@';
	_charact[1][3] = '@';
	_charact[1][4] = ' ';

	_charact[2] = new char [4];
	_charact[2][1] = '[';
	_charact[2][2] = '#';
	_charact[2][3] = ']';
	_charact[2][4] = '>';

	_charact[3] = new char [4];
	_charact[3][1] = '@';
	_charact[3][2] = '@';
	_charact[3][3] = '@';
	_charact[3][4] = ' ';
}

Character::Character(Character const &src)
{
	*this = src;
}

Character& Character::operator=(Character const &rhs) {
	if(this != &rhs)
	{
		this->_charact = rhs._charact;
		this->_x = rhs._x;
		this->_y = rhs._y;
	}
	return (*this);
}

void Character::setPos(int x, int y)
{
	this->_x = x;
	this->_y = y;
}

int& Character::constGetY() {
	return this->_y;
}

char** Character::getPlayer() {
	return this->_charact;
}

int		Character::getHP() {
	return this->_HP;
}

int Character::getScore() {
	return this->_score;
}

int Character::getAmmo() {
	return this->_ammo;
}

void Character::setHP(int HP) {
	this->_HP -= HP;
}

Character::~Character()
{
	delete []_charact [1];
	delete []_charact [2];
	delete []_charact [3];
	delete []_charact;
}

void Character::shotLine(char **buff) {
	_ammo -= 1;
	for (int x = 5; x < WIN_W; x++)
	{
		if (buff[_y+1][x] == '+')
			_HP++;
		if (buff[_y+1][x] == '=')
			_ammo += 3;
		if (buff[_y+1][x] == '<')
			_score += 10;
		if (buff[_y+1][x] == '*')
			_score += 5;
		buff[_y+1][x] = '-';
	}
}
