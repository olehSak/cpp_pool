/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Drawer.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apluzhni <apluzhni@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/06 13:54:00 by apluzhni          #+#    #+#             */
/*   Updated: 2019/10/06 19:10:44 by apluzhni         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Drawer.hpp"

/* ~ CONSTRUCTORS ~ */

Drawer::Drawer(): run(true)
{
	noecho();
	cbreak();
	nWindow = subwin(stdscr, WIN_H, WIN_W, 0, 0);
	keypad(nWindow, TRUE);
	nodelay(nWindow, TRUE);
	fillBuff();
	werase(nWindow);
	box(nWindow, 0, 0);
	drawBuff();
	for (int i = 0; i < 50; i++)
		buff[getRand(34)][getRand(90)] = '.';
	wrefresh(nWindow);
}

Drawer::~Drawer()
{
	for (int y = 0; y < WIN_H - 1; y++) {
		delete[] buff[y];
	}
	delete[] buff;
}

/* ~ FUNCTIONS ~ */

void	Drawer::loadScreen()
{
	werase(nWindow);
	box(nWindow, 0, 0);
	mvwprintw(nWindow, 2, 7, "                                                         */////////**");
	mvwprintw(nWindow, 3, 7, "                                                    ,//,/((///((./** ");
	mvwprintw(nWindow, 4, 7, "                               ..,,,*,,,.        /*(//////////(*/*   ");
	mvwprintw(nWindow, 5, 7, "                           ,,**//////////**,,    **,.....,*//(,*,    ");
	mvwprintw(nWindow, 6, 7, "                        ,**///(((((((((((////*,.       ..*//*/,      ");
	mvwprintw(nWindow, 7, 7, "                      ,*//(((((((########((/////,     .*/(*/,        ");
	mvwprintw(nWindow, 8, 7, "                    ,//((((((##############((((//*  .,/(,*.          ");
	mvwprintw(nWindow, 9, 7, "                   *//((((###############((##(((/,.*/(**             ");
	mvwprintw(nWindow, 10, 7, "                  ,//((##############(#######(*./(//,*))))           ");
	mvwprintw(nWindow, 11, 7, "                  /(((############((##/#/#/###(,,/((/*               ");
	mvwprintw(nWindow, 12, 7, "                 */(((############/#/##/#/#(,*/((/(//*               ");
	mvwprintw(nWindow, 13, 7, "                 */(((#####(###/#/##/#/#(**/((/((((//,               ");
	mvwprintw(nWindow, 14, 7, "               ,/*/(((((###/#/#/##/#(,*/((/(###(((/*                 ");
	mvwprintw(nWindow, 15, 7, "             **//*/(((##/#/#/#/##*.//((/######(((//                  ");
	mvwprintw(nWindow, 16, 7, "          ,/*//,.  /((######/.,//(((#########((/*                    ");
	mvwprintw(nWindow, 17, 7, "        *////*.     /((/*.,//((/(#########(((//.                     ");
	mvwprintw(nWindow, 18, 7, "      *////*...     ..*///((/###########(((/*.                       ");
	mvwprintw(nWindow, 19, 7, "    */,///**....,/////*/*/((((((((((((((/*.                          ");
	mvwprintw(nWindow, 20, 7, "  ,**///////////(,//,       .*//////,.                               ");
	mvwprintw(nWindow, 21, 7, " **/.((((((*,//*                                                     ");
	mvwprintw(nWindow, 22, 7, ",**//////**.                                                         ");

	mvwprintw(nWindow, 25, 12, "  _____ ______ _______   _____  ______          _______     ___ ");
	mvwprintw(nWindow, 26, 12, " / ____|  ____|__   __| |  __ \\|  ____|   /\\   |  __ \\ \\   / / |");
	mvwprintw(nWindow, 27, 12, "| |  __| |__     | |    | |__) | |__     /  \\  | |  | \\ \\_/ /| |");
	mvwprintw(nWindow, 28, 12, "| | |_ |  __|    | |    |  _  /|  __|   / /\\ \\ | |  | |\\   / | |");
	mvwprintw(nWindow, 29, 12, "| |__| | |____   | |    | | \\ \\| |____ / ____ \\| |__| | | |  |_|");
	mvwprintw(nWindow, 30, 12, " \\_____|______|  |_|    |_|  \\_\\______/_/    \\_\\_____/  |_|  (_)");

	wrefresh(nWindow);
	napms(3000);
}

void	Drawer::events(void)
{
	if (player.getHP() == 0)
	{
		loadScreen();
		run = false;
	}
	switch (wgetch(nWindow))
	{
		case 27:
		{
			run = false;
			break;
		}
		case KEY_UP:
		{
			if (player.constGetY() > 1)
			{
				cleanPlayer();
				player.setPos(0, player.constGetY() - 1);
				drawPlayer();
				wrefresh(nWindow);
			}
			break;
		}
		case KEY_DOWN:
		{
			if (player.constGetY() < WIN_H - 4)
			{
				cleanPlayer();
				player.setPos(0, player.constGetY() + 1);
				drawPlayer();
				wrefresh(nWindow);
			}
			break;
		}
		case 32:
		{
			player.shotLine(buff);
			break;
		}
	}
	generateRow();
	moveBack();
}

void	Drawer::fillBuff(void)
{
	buff = new char* [WIN_H - 1];

	for (int y = 0; y < WIN_H - 1; y++) {
		buff[y] = new char[WIN_W - 1];
		for (int x = 0; x < WIN_W - 1; x++)
			buff[y][x] = ' ';
	}

	buff[player.constGetY()][1] = player.getPlayer()[1][1];
	buff[player.constGetY()][2] = player.getPlayer()[1][2];
	buff[player.constGetY()][3] = player.getPlayer()[1][3];
	buff[player.constGetY()][3] = player.getPlayer()[1][4];

	buff[player.constGetY()+1][1] = player.getPlayer()[2][1];
	buff[player.constGetY()+1][2] = player.getPlayer()[2][2];
	buff[player.constGetY()+1][3] = player.getPlayer()[2][3];
	buff[player.constGetY()+1][4] = player.getPlayer()[2][4];

	buff[player.constGetY()+2][1] = player.getPlayer()[3][1];
	buff[player.constGetY()+2][2] = player.getPlayer()[3][2];
	buff[player.constGetY()+2][3] = player.getPlayer()[3][3];
	buff[player.constGetY()+2][3] = player.getPlayer()[3][4];
}

void	Drawer::drawBuff(void)
{
	mvwprintw(nWindow, 34, 5, "Score: %02d", player.getScore());
	mvwprintw(nWindow, 34, 20, "HP: %02d", player.getHP());
	mvwprintw(nWindow, 34, 30, "Ammo: %02d", player.getAmmo());
	for (int y = 1; y < WIN_H - 1; y++)
		for (int x = 1; x < WIN_W - 1; x++)
			mvwaddch(nWindow, y, x, buff[y][x]);
}

void	Drawer::cleanPlayer(void)
{
	for (int y = player.constGetY(); y <= player.constGetY()+2; y++)
		for (int x = 1; x <= 4; x++) {
			if(buff[y][x] == '<' || buff[y][x] == '*')
				player.setHP(1);
			buff[y][x] = ' ';
		}
	for (int y = player.constGetY(); y <= player.constGetY()+2; y++)
		for (int x = 1; x <= 4; x++)
			mvwaddch(nWindow, y, x, buff[y][x]);
}

void	Drawer::drawPlayer(void)
{
	buff[player.constGetY()][1] = player.getPlayer()[1][1];
	buff[player.constGetY()][2] = player.getPlayer()[1][2];
	buff[player.constGetY()][3] = player.getPlayer()[1][3];
	buff[player.constGetY()][3] = player.getPlayer()[1][4];

	buff[player.constGetY()+1][1] = player.getPlayer()[2][1];
	buff[player.constGetY()+1][2] = player.getPlayer()[2][2];
	buff[player.constGetY()+1][3] = player.getPlayer()[2][3];
	buff[player.constGetY()+1][4] = player.getPlayer()[2][4];

	buff[player.constGetY()+2][1] = player.getPlayer()[3][1];
	buff[player.constGetY()+2][2] = player.getPlayer()[3][2];
	buff[player.constGetY()+2][3] = player.getPlayer()[3][3];
	buff[player.constGetY()+2][3] = player.getPlayer()[3][4];

	for (int y = player.constGetY(); y <= player.constGetY()+2; y++)
		for (int x = 1; x <= 4; x++)
			mvwaddch(nWindow, y, x, buff[y][x]);
}

int		Drawer::getRand(int limit)
{
	int i = rand() % 10000;
	return (i%limit);
}

void	Drawer::moveBack(void)
{
	for (int y = 0; y < WIN_H - 1; y++)
		for (int x = 1; x < WIN_W - 1; x++)
			if ((buff[y][x] == '.'
				 || buff[y][x] == '*'
				 || buff[y][x] == '<'
				 || buff[y][x] == '+'
				 || buff[y][x] == '-'
				 || buff[y][x] == '=')
				&& buff[y][x - 1] == ' ')
			{
				if (x - 1 > 0)
					buff[y][x - 1] = buff[y][x];
				else
					buff[y][x - 1] = ' ';

				if(buff[player.constGetY()][3] == '<'
				|| buff[player.constGetY() + 1][5] == '<'
				|| buff[player.constGetY() + 2][3] == '<')
					player.setHP(1);

				buff[player.constGetY()][3] = ' ';
				buff[player.constGetY() + 1][5] = ' ';
				buff[player.constGetY() + 2][3] = ' ';

				buff[y][x] = ' ';
			}
	drawBuff();
}

void		Drawer::generateRow(void)
{
	for(int y = 0; y < WIN_H - 1; y++)
	{
		buff[y][WIN_W - 2] = 0;
		for (int i = 0; i < 34; i++) {
			int numb = std::rand() % 40;
			if (numb == 1)
				buff[y][WIN_W - 2] = '.';
			else
				buff[y][WIN_W - 2] = ' ';

			int numb2 = std::rand() % 1000;

			if (numb2 == 1)
				buff[y][WIN_W - 2] = '*';
			else if (numb2 == 9)
				buff[y][WIN_W - 2] = '<';
			else if (numb2 == 20)
				buff[y][WIN_W - 2] = '+';
			else if (numb2 == 17)
				buff[y][WIN_W - 2] = '=';
		}
	}
}

Drawer::Drawer(Drawer const &src) {
	*this = src;
}

Drawer& Drawer::operator=(Drawer const &rhs) {
	if(this != &rhs)
	{
		this->nWindow = rhs.nWindow;
		this->buff = rhs.buff;
		this->player = rhs.player;
	}
	return (*this);
}
