//
// Created by Oleh SAK on 2019-10-06.
//

#ifndef CPP_POOL_CHARACTER_HPP
#define CPP_POOL_CHARACTER_HPP

#include "Drawer.hpp"

class Character  {
protected:
	char  		**_charact;
	int 		_x;
	int 		_y;
	int 		_HP;
	int 		_score;
	int			_ammo;
public:
	Character();
	~Character();
	Character(Character const &src);

	Character &	operator=(Character const & rhs);

	void	setPos(int x, int y);
	int 	&constGetY();
	char 	**getPlayer();

	int 	getHP();
	int		getAmmo();
	int		getScore();

	void	setHP(int HP);

	void	shotLine(char **buff);

};


#endif //CPP_POOL_CHARACTER_HPP
