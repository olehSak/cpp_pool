#ifndef CPP_POOL_CONVERT_HPP
#define CPP_POOL_CONVERT_HPP

#include <iostream>
#include <string>

class Convert {
private:
	int		_iType;
	float 	_fType;
	char 	_cType;
	double 	_dType;

	char 	_type;

	std::string	_sType;

public:
	Convert(std::string str);
	~Convert();

	char		toChar(std::string str);
};


#endif //CPP_POOL_CONVERT_HPP
