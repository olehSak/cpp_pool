#include <iostream>
#include <string>
#include <time.h>
#include <stdlib.h>
#include "serialize.hpp"

int			main()
{
	srand(time(NULL));
	void		*buff = serialize();
	Data		*raw = deserialize(buff);
	std::cout	<< "s1: " <<raw->s1 << std::endl
				<< "n: " << raw->n << std::endl
				<< "s2: " << raw->s2
				<< std::endl;
	return 0;
}