#ifndef SUPERSTRING_HPP
# define SUPERSTRING_HPP

#include <string>
#include <cstring>
#include <iostream>

class SuperString {
	private:
		std::string _str;
	public:

	float 		_fl;
	int 		_check;

	explicit SuperString(std::string const &data);
	SuperString(SuperString const &src);
	~SuperString();

	SuperString &operator=(SuperString const &rhs);



	class					CantConvertException: public std::exception
	{
		public:
			CantConvertException();
			~CantConvertException() throw();
			char const				*what() const throw();
	};


	operator std::string const &() const;
	operator char() const;
	operator int() const;
	operator float() const;
	operator double() const;
};


#endif
