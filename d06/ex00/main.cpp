#include "SuperString.hpp"

#include <iostream>
#include <cctype>
#include <iomanip>
#include <cmath>

void outConvert(SuperString &str) {
	std::cout << "char: ";
	try {
		char ch = static_cast<char>(str);

		if (isprint(ch)) {
			std::cout << ch << std::endl;
		}
		else
		{
			if(isascii(ch))
				std::cout << "Non displayable" << std::endl;
			else
				std::cout << "impossible" << std::endl;
		}
	} catch (SuperString::CantConvertException &e) {
		std::cout << e.what() <<std::endl;
	}

	std::cout << "int: ";
	try {
		int integ = static_cast<int>(str);
		std::cout << integ << std::endl;
	} catch (SuperString::CantConvertException &e) {
		std::cout << e.what() <<std::endl;
	}

	std::cout << std::fixed;
	std::cout.precision(1);

	std::cout << "float: ";
	try {
		float fnum = static_cast<float>(str);

		if(str._check)
			str._fl = fnum;

		if (fnum > 0 &&  std::isinf(fnum))
			std::cout  << "+";
		std::cout << fnum << "f" <<  std::endl;
	} catch (SuperString::CantConvertException &e) {
		std::cout << e.what() <<std::endl;
	}

	std::cout << "double: ";
	try {
		double dnum = static_cast<double>(str);
		if(str._check)
			dnum = static_cast<double>(str._fl);
		if (dnum > 0 && std::isinf(dnum))
			std::cout  << "+";
		std::cout << dnum <<  std::endl;
	} catch (SuperString::CantConvertException &e) {
		std::cout << e.what() <<std::endl;
	}
}

int main(int argc, char **argv) {
	if (argc == 2)
	{
		SuperString sString(argv[1]);
		outConvert(sString);
	}
}